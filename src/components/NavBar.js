import React, {Fragment,useContext,useEffect} from 'react'
import {Navbar,Container,Nav,NavDropdown} from 'react-bootstrap'
import UserContext from "./../UserContext"
import {useCart} from "./CartContext"


export default function NavBar() {
  const items = useCart()
  const {user,setUser} = useContext(UserContext)
 
  // useEffect(()=>{
  //     console.log(user.email)
  // },[])

  const token=localStorage.getItem("token")
  const admin = localStorage.getItem("admin")
  const email = localStorage.getItem("email")
  return (
    <Navbar bg="light" expand="lg" className="mx-1">
    <Container  className="mx-2">
        <Navbar.Brand href="/">HoneyBee</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
            <Nav.Link href="/">Welcome {email}</Nav.Link>
            <Nav.Link href="/">Home</Nav.Link>

                {
                admin == "true"
                ?
                <Fragment>
                <Nav.Link href="/Orders">Orders</Nav.Link>
                <Nav.Link href="/productList">Product List</Nav.Link>
                <Nav.Link href="/newProduct">Add New Product</Nav.Link>
                </Fragment>
                :
                <Fragment>
                <Nav.Link href="/products">Products</Nav.Link>
                <Nav.Link href="/MyOrders">MyOrders</Nav.Link>
                <Nav.Link href="#">Cart ({items.length})</Nav.Link>
                </Fragment>

              }
              { 
             token !== null
                   ? 
                   <Nav.Link href="/logout" className="text-dark">Logout</Nav.Link> 
                   :
                   <Fragment>
                      <Nav.Link href="/login" className="text-dark">Login</Nav.Link>
                      <Nav.Link href="/register" className="text-dark">Register</Nav.Link>
                   </Fragment>
                }
            <NavDropdown title="Others" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">People</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Events</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Promos</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">About Us</NavDropdown.Item>
            </NavDropdown>

        </Nav>
        </Navbar.Collapse>
    </Container>
    </Navbar>
  )
}
