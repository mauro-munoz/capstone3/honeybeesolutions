import React, {useContext,useState,Fragment,useEffect} from 'react'
import OrderContext from "./../OrderContext"
import {useCart, useDispatchCart} from './CartContext'

const CartItem = ({ product, index, handleRemove }) => {
    return (
      <article>
        <div className="dt w-100 bb b--black-10 pb2 mt2 dim blue" href="#0">
          <div className="dtc w3">
            <img src={product.imageUrl} className="db w-100" />
          </div>
          <div className="dtc v-top pl2">
            <h1 className="f6 f5-ns fw6 lh-title black mv0">{product.productName}</h1>
           
            <dl className="mt2 f6">
              <dt className="clip">Price</dt>
              <dd className="ml0">
                {product.price.toLocaleString("en", {
                  style: "currency",
                  currency: "PHP"
                })}
              </dd>
            </dl>
            <button onClick={() => handleRemove(index)}>Remove from cart</button>
          </div>
        </div>
      </article>
    );
  };

export default function Cart() {
    
    
    const dispatch = useDispatchCart()
    const handleRemove = index => {
        dispatch({type: 'REMOVE', index})
    }
    const {order,setOrder} = useContext(OrderContext)
    
    // console.log(order)
    const items = useCart()
    
    const totalPrice = items.reduce((total, b) => total + b.price, 0)

  return (
   <div>
       <div><h2>Cart</h2></div>
       <div>
           {
               items.length == 0 ? <div>Cart is Empty</div> : 
               <div>
                 <main>
                    <p>
                        Total price:{" "}
                        {totalPrice.toLocaleString("en", {
                        style: "currency",
                        currency: "USD"
                        })}
                    </p>
                    {items.map((item, index) => (
                        <CartItem
                        handleRemove={handleRemove}
                        key={index}
                        product={item}
                        index={index}
                        />
                    ))}
                    </main>
               </div>
           }
       </div>
   </div>
  )
}
