import React from 'react'
import {Card,ListGroup,ListGroupItem} from 'react-bootstrap'

export default function OrderCard({orderProp}) {
    const {totalAmount,productName,quantity,OrderDate,userId,_id} = orderProp
    return (
        <div className="d-inline-flex">
          <Card className="m-2" style={{ width: '18rem' }}>
          <Card.Img variant="top" src="https://www.fillmurray.com/640/360" />
          <Card.Body>
              <Card.Title>Order Reference: {_id}</Card.Title>
              <Card.Text>
                  Ordered Date: {OrderDate}
              </Card.Text>
              <Card.Text>
                  Product: {productName}
              </Card.Text>
          </Card.Body>
          <ListGroup className="list-group-flush">
              <ListGroupItem>Total Amount: {totalAmount}</ListGroupItem>
              <ListGroupItem>Quantity: {quantity}</ListGroupItem>
              <ListGroupItem>User Id: {userId}</ListGroupItem>
          </ListGroup>
          </Card>
        </div>
  
    )}
