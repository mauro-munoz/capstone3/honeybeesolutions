import React from 'react'

export default function Footer() {
  return (
    <div className="footer d-flex justify-content-center align-items-center text-dark" style={{height: '5vh'}}>
    <p className="m-0 font-weight-bold">Made by: Mauro III Munoz &#64; HoneyBee Eco-Solutions | by Mrs. Bee &#169;</p>
  </div>
  )
}
