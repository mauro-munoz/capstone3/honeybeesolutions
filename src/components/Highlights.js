import React from 'react'
import {Carousel} from 'react-bootstrap'
export default function Highlights() {
  return (
                <Carousel className="my-2">
            <Carousel.Item>
                <img
                className="d-block w-50  mx-auto"
                src="https://www.fillmurray.com/640/360"
                alt="First slide"
                />
                <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-50 mx-auto"
                src="https://www.fillmurray.com/640/360"
                alt="Second slide"
                />

                <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-50  mx-auto"
                src="https://www.fillmurray.com/640/360"
                alt="Third slide"
                />

                <Carousel.Caption>
                <h3>Third slide label</h3>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </Carousel.Caption>
            </Carousel.Item>
            </Carousel>
  )
}
