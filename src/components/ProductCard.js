import React,{useContext, Fragment,useState} from 'react'
import {Card,ListGroup,ListGroupItem} from 'react-bootstrap'
import UserContext from "./../UserContext"
import img1 from './../images/black.jpg'
import { useParams, useNavigate, Link } from 'react-router-dom'
import OrderContext from "./../OrderContext"
import {useDispatchCart} from './CartContext'


export default function ProductCard(prop) {
    const {productProp, onAdd } = prop
    const {price,productName,description,pictureName,_id,isOffered} = productProp
    const {user,setUser} = useContext(UserContext)
    // console.log(productProp)
    const archiveProduct = (_id) => {
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/archive/${_id}`,{
            method: "PUT",
            headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}

        }).then(response => response.json()).then(response => {
			console.log(response)})
    }
    const deleteProduct = (_id) => {
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/delete/${_id}`,{
            method: "DELETE",
            headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}

        }).then(response => response.json()).then(response => {
			console.log(response)})
    }

    const unarchiveProduct = (_id) => {
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/unarchive/${_id}`,{
            method: "PUT",
            headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}

        }).then(response => response.json()).then(response => {
			console.log(response)})
    }
    // const [cartItems, setCartItems] = useState([])
    const {order,setOrder} = useContext(OrderContext)
    const dispatch = useDispatchCart()
    const addToCart = (item) => {
        console.log(item)
        dispatch({type:"ADD",item})
    }
    
  return (
      <div className="d-inline-flex">
        <Card className="m-2" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={img1} alt={img1}/>
        <Card.Body>
            <Card.Title>{productName}</Card.Title>
            <Card.Text>
                {description}
            </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
            <ListGroupItem>Php {price}</ListGroupItem>
            <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
            <ListGroupItem>Vestibulum at eros</ListGroupItem>
        </ListGroup>
        <Card.Body>
            {
                user.isAdmin 
                ?
                <Fragment>
                    
                    {
                        isOffered
                        ?
                        <Card.Link href="#" onClick={ () => archiveProduct(_id)}> Archive</Card.Link>
                        :
                        <Card.Link href="#" onClick={ () => unarchiveProduct(_id)}> Unarchive</Card.Link>
                    }
                    
                    <Card.Link href="#" onClick={ () => deleteProduct(_id)}>Delete</Card.Link>
                    <Link to={`/updateProduct/${_id}`}>Update</Link>
                </Fragment>
                :
                <Fragment>
                    <Link to={`/view/${_id}`}>View Product</Link>
                    <Card.Link href="#" onClick={()=> addToCart(productProp)}>Add To Cart</Card.Link>
                </Fragment>
            }
           
        </Card.Body>
        </Card>
      </div>

  )
}
