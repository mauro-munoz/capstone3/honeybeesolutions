import React from 'react'
import img1 from "./../images/honeybee.jpg"

export default function Banner({bannerProp}) {
  const {title, description,buttonDesc,buttonTitle,buttonLink} = bannerProp
  return (
    <div className="jumbotron jumbotron-fluid">
        <div className="d-flex px-3">
            <div>
                <img className="logo px-1 img-fluid" src={img1}/>
            </div>
            <div>
            <h1 className="display-4">{title}</h1>
            <p className="lead">{description}</p>
            </div>
        </div>
        <hr className="my-3"/>
        <p className="text-center">{buttonDesc}</p>
        <div className="d-flex justify-content-center">
        <a className="btn btn-info btn-sm " href={buttonLink} role="button">{buttonTitle}</a>
        </div>
        
    </div>
  )
}
