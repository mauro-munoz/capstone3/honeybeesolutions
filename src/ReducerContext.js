import {createContext} from 'react'

const ReducerContext = createContext()

export const ReducerProvider = ReducerContext.Provider

export default ReducerContext
