import React, { useEffect,useState,useContext } from 'react'
import {Form,Button,Row,Col} from 'react-bootstrap'
import ProductContext from "./../ProductContext"
import { useParams, useNavigate, Link } from 'react-router-dom'

export default function UpdateProduct() {
    // useEffect(()=>{},[])
    const navigate = useNavigate()
    const { productId } = useParams()
	console.log(productId)
    const {product,setProduct} = useContext(ProductContext)

    let [pname, setPName]= useState("")
    let [description, setDescription]= useState("")
    let [price, setPrice]= useState("")
    
    // function updateProduct (product._id){
    //     console.log(product._id)
    // }

    const updateProduct = (productId) => {
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/product-update/${productId}`,{
            method:`PUT`,
            headers: {
                "Content-Type":"application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productName: pname,
                price: price,
                description :description,
            })
        }).then(result => result.json().then(result =>{
            console.log(result)
            navigate(`/productList`)
          }
          ))
    }
        //post to API

        

  return (
    <Row className ="justify-content-center">
    <Col xs={12} md={6}>
        <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Product name</Form.Label>
            <Form.Control type="text" placeholder="Enter product name"  value ={pname}
                    onChange={(e)=> setPName (e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Product Description</Form.Label>
            <Form.Control type="text" placeholder="Enter product description" value ={description}
                    onChange={(e)=> setDescription (e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" placeholder="Enter price"value ={price}
                    onChange={(e)=> setPrice (e.target.value)} />
        </Form.Group>
        <Button variant="primary" onClick={ () => {updateProduct(productId)}}>
            Submit
        </Button>
        </Form>
    </Col>
    </Row>
   
  )
}
