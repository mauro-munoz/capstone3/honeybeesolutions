import React, { useEffect, useContext } from 'react'
import OrderContext from "./../OrderContext"
import OrderCard from './../components/OrderCard'
export default function Orders() {
    const {order,setOrder} = useContext(OrderContext)
    useEffect(()=>{
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/orders`, {
            method:"GET",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        }).then(result => result.json()).then(result=>{
           setOrder(result)
        })
    },[])
    console.log(order)
    const ordersCard = order.map(card=>{
        return <OrderCard key={card._id} orderProp={card}/>
      })
  return (
   <div>{ordersCard}</div>
  )
}
