import React,{Fragment} from 'react'
import Banner from './../components/Banner';
import Highlights from './../components/Highlights'
import "./../styles.css"

export default function HomePage() {
    const HomePageInfo = {
        title: "HoneyBee Eco-Solutions",
        description:"Eco-friendly alternatives for everyday use.",
        buttonDesc:"Learn more about us!",
        buttonTitle:"About Us",
        buttonLink:"/"
    }
  return (
    <Fragment>
      <Banner bannerProp={HomePageInfo} />
      <Highlights/>
    </Fragment>
    
  )
}
