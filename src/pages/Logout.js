import React, {useEffect,useState,useContext} from 'react'
import {useNavigate} from 'react-router-dom'
import UserContext from "./../UserContext"

export default function Logout() {
    localStorage.clear()
    const navigate= useNavigate()
    const {user,setUser} = useContext(UserContext)
    useEffect (()=>{
      setUser({
        id:null,
        isAdmin:null
      })
      // console.log(user.id)
      navigate(`/login`)
    })
   
  return (
    <div>Logout</div>
  )
}
