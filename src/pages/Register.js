import React, {useEffect, useState,useContext} from 'react'
import {Form, Button,Row,Col,Container} from 'react-bootstrap'
import {useNavigate} from "react-router-dom"
import UserContext from "./../UserContext"

export default function Register() {
    const navigate = useNavigate()
   
    let [fullName, setfullName]= useState("")
    let [email, setemail]= useState("")
    let [password, setpassword]= useState("")
    let [vpassword, setvpassword]= useState("")
    let [isDisabled, setisDisabled]=useState(true)
    // useEffect(function,options)
    useEffect(()=>{console.log(fullName)},[fullName])
    useEffect(()=>{console.log(email)},[email])
    useEffect(()=>{console.log(password)},[password])
    useEffect(()=>{console.log(vpassword)},[vpassword])
    
    useEffect(()=>{
        if(fullName !== "" && email !== "" && password !== "" && vpassword !== "" && password == vpassword){
            setisDisabled(false)
        }
    })

    const registerUser = (e) => {
        e.preventDefault()

        //post to API
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/users/add-user`,{
            method:`POST`,
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                fullName: fullName,
                email: email,
                password: password
            })
        }).then(result => result.json().then(result =>{
            alert(result.message)
            navigate(`/login`)
        
        }
            ))
    }

    const {user} = useContext(UserContext)
    // prevents logged in user to go to register page, User must logout first
        useEffect(()=>{
            if(user.fullName !== undefined){
               navigate(`/products`)
               }},[user.fullName]) 

            return (
                <Container className="m-5">
                    <h2 className ="d-flex justify-content-center">Register</h2>
                    <p className="text-center">Already registered? <a href="/login">Login</a></p>
                    <Row className ="justify-content-center">
                    <Col xs={12} md={6}>
                        <Form className="m-4" onSubmit={(e)=>registerUser(e)}>
                            <Form.Group className="mb-3" controlId="FullName">
                                <Form.Label>Full Name</Form.Label>
                                <Form.Control type="text" placeholder="Full Name" value ={fullName} onChange={(e)=> setfullName (e.target.value)}/>
                            </Form.Group>
            
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" value ={email}
                                onChange={(e)=> setemail (e.target.value)} />
                                <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>
            
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" value={password} 
                                    onChange={(e)=> setpassword (e.target.value)}
                                />
                            </Form.Group>
            
                            <Form.Group className="mb-3" controlId="verifyPassword">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Control type="password" placeholder="Password"  value={vpassword}
                                onChange={(e)=> setvpassword (e.target.value)} />
                            </Form.Group>
            
                            <Button  className="btn btn-info btn-sm d-block mx-auto" type="submit" disabled={isDisabled}>
                                Submit
                            </Button>
            
                        </Form>
                    </Col>
                </Row>
                </Container>
            )
}
