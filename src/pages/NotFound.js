import React from 'react'
import Banner from './../components/Banner'

export default function NotFound() {
    const NotFoundInfo =  {
        title: "Page Not Found",
        description:"Error 404",
        buttonDesc:"Go Back To Homepage",
        buttonTitle:"Home",
        buttonLink:"/"
    }

  return (
    <Banner bannerProp={NotFoundInfo}/>
  )
}
