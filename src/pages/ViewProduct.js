import React, {useContext,useEffect,Fragment} from 'react'
import ProductContext from "./../ProductContext"
import Banner from './../components/Banner'
import { useParams, useNavigate, Link } from 'react-router-dom'

export default function ViewProduct() {
  const {product,setProduct} = useContext(ProductContext)
  const navigate = useNavigate()
	const { productId } = useParams()
	console.log(productId)


    useEffect(() =>{
      fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/${productId}`,{
        method:"GET",
        headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
                }}).then(result => result.json()).then(result => {setProduct({productDetail:result,loading:false})
            }) 
    },[product])

    
console.log(product.productDetail)
const {productName,price,description} = product.productDetail
const quantity = document.getElementById('quantity')
const buyProduct = () => {
  fetch(`https://zuitt-capstone2-mauro.herokuapp.com/orders/add-order`,{
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({
      productId: productId,
      productName: productName,
              quantity: quantity.value
    })
  })		.then(result => result.json())
  .then(result => {
    // console.log(result)

    if(result){
      alert('Thank you for your purchase.')
      navigate(`/MyOrders`)
    } else {
      alert('Something went wrong.')
    }
  })
}
  return (
    <Fragment>
        <div className="jumbotron jumbotron-fluid">
        <div className="d-flex px-3">
            <div>
                <img className="logo px-1 img-fluid" />
            </div>
            <div>
            <h1 className="display-4">{productName}</h1>
            <p className="lead">{description}</p>
            </div>
        </div>
        <hr className="my-3"/>
        <div class="form-group">
            <label for="quantity">Quantity:</label>
            <input type="number" id="quantity" required class="form-control"></input>
        </div>

        <p className="text-center">Buy Now!</p>
        <div className="d-flex justify-content-center">
        <a className="btn btn-info btn-sm " onClick = {()=>buyProduct()} role="button">Php {price}</a>
        </div>
        
    </div>
    </Fragment>
  )
}
