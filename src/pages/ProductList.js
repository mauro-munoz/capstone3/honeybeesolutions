import React,{useContext} from 'react'
import ProductContext from "./../ProductContext"
import ProductCard from './../components/ProductCard'
export default function ProductList() {
    const {product,setProduct} = useContext(ProductContext)
    fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/`,{
        method:"GET",
        headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
                }}).then(result => result.json()).then(result => {setProduct({productDetail:result,loading:false})
            }) 

    const productsCard = product.productDetail.map(card=>{
        return <ProductCard key={card._id} productProp={card}/>
        })
  return (
    <div>{productsCard}</div>
  )
}
