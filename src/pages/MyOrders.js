import React, { useEffect, useContext } from 'react'
import OrderContext from "./../OrderContext"
import OrderCard from './../components/OrderCard'
import {useNavigate} from "react-router-dom"
export default function Orders() {
    const navigate = useNavigate()
    const {order,setOrder} = useContext(OrderContext)
    useEffect(()=>{
        if(token === null){
            navigate(`/`)
          }
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/orders/user-orders`, {
            method:"GET",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        }).then(result => result.json()).then(result=>{
           setOrder(result)
        })
    },[])
    console.log(order)
    const ordersCard = order.map(card=>{
        return <OrderCard key={card._id} orderProp={card}/>
      })
      const token=localStorage.getItem("token")

  return (
      ordersCard.length>0 ?
   <div>{ordersCard}</div>
   :
   <div>You have no orders</div>
  )
}
