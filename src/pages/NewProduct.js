import React, { useEffect,useState } from 'react'
import {Form,Button,Row,Col} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function NewProduct() {
    // useEffect(()=>{},[])
    const navigate = useNavigate()
    let [pname, setPName]= useState("")
    let [description, setDescription]= useState("")
    let [price, setPrice]= useState("")
    const registerUser = (e) => {
        e.preventDefault()

        //post to API
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/add-product`,{
            method:`POST`,
            headers: {
                "Content-Type":"application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productName: pname,
                price: price,
                description :description,
            })
        }).then(result => result.json().then(result =>{
            alert(result.message)
            alert(`You will be redirected back to Products`)
            navigate(`/productList`)
          })).catch(err=>console.log(err))
    }

  return (
    <Row className ="justify-content-center">
    <Col xs={12} md={6}>
        <Form onSubmit={(e)=>registerUser(e)}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Product name</Form.Label>
            <Form.Control type="text" placeholder="Enter product name"  value ={pname}
                    onChange={(e)=> setPName (e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Product Description</Form.Label>
            <Form.Control type="text" placeholder="Enter product description" value ={description}
                    onChange={(e)=> setDescription (e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" placeholder="Enter price"value ={price}
                    onChange={(e)=> setPrice (e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit">
            Submit
        </Button>
        </Form>
    </Col>
    </Row>
   
  )
}
