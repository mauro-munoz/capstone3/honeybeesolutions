import React ,{Fragment,useContext,useEffect,useState} from 'react'
import ProductCard from './../components/ProductCard'
import Banner from './../components/Banner'
import Cart from './../components/Cart'
import UserContext from "./../UserContext"
import ProductContext from "./../ProductContext"
import {useNavigate} from "react-router-dom"


export default function Products(prop) {


  const {onAdd} = prop
  const ProductInfo = {
        title: "Product Page",
        description:"Have a look at our products that save the earth",
        buttonDesc:"Go back to our Home Page",
        buttonTitle:"Home",
        buttonLink:"/"
  }

  const {user,setUser} = useContext(UserContext)
// console.log(product)

    //Fetch Products from API 
    const {product,setProduct} = useContext(ProductContext)

    fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/active-products`,{
      method:"GET",
      headers:{
      "Authorization": `Bearer ${localStorage.getItem("token")}`
              }}).then(result => result.json()).then(result => {setProduct({productDetail:result,loading:false})
          }) 
  //map product to card 
// useEffect(()=>{console.log(product.productDetail)},[])

const coursesCard = product.productDetail.map(card=>{
  return <ProductCard key={card._id} productProp={card} onAdd = {onAdd}/>
})
//Cart 

const [cartItems, setCartItems] = useState([])
// console.log(cartItems)




  return (
    <Fragment>
        <Banner bannerProp={ProductInfo}/>
        <div className="d-flex">
        {coursesCard}
        <div><Cart cartItems={cartItems}/></div>
        </div>

    </Fragment>
   
  )
}
