import React, {useEffect, useState,useContext} from 'react'
import {Form, Button,Row,Col,Container} from 'react-bootstrap'
import {useNavigate} from "react-router-dom"
import UserContext from "./../UserContext"
import ReducerContext from "./../ReducerContext"
import ProductContext from "./../ProductContext"
export default function Login() {

   
 
    const navigate = useNavigate()
    let [email, setemail]= useState("")
    let [password, setpassword]= useState("")
    let [isDisabled, setisDisabled]=useState(true)
    // useEffect(function,options)
    // useEffect(()=>{console.log(email)},[email])
    // useEffect(()=>{console.log(password)},[password])
    const {user,setUser} = useContext(UserContext)
    const {product,setProduct} = useContext(ProductContext)
    const { state, dispatch } = useContext(ReducerContext)
    useEffect(()=>{
        if( email !== "" && password !== "" ){
            setisDisabled(false)
        }})
    const registerUser = (e) => {
        e.preventDefault()
   
        //post to API
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/users/login`,{
            method:`POST`,
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(result => result.json().then(result =>{
            
            if(result.message == `User does not exist`){
                alert(`Something went wrong: ${result.message}`)
                navigate(`/register`)
            }if(result.message == `auth failed`) {
                alert(result.message)
                // dispatch({type: "USER", payload: true})
            }
            if(result.message!=`User does not exist`&&result.message != `auth failed`){

               

                localStorage.setItem('token', result.message)
                localStorage.setItem('email', email)

                fetch('https://zuitt-capstone2-mauro.herokuapp.com/users/profile', {
				      method: "GET",
				      headers:{
				        "Authorization": `Bearer ${localStorage.getItem("token")}`
				      }
				    })
				    .then(response => response.json())
				    .then(response => {
				      
				      localStorage.setItem('admin', response.isAdmin)

				      dispatch({type: "USER", payload: true})
				        
				    })

                
                // console.log(email)
                alert(`User successfully logged in`)
               
                navigate(`/`)
             

                // fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/active-products`,{
                // method:"GET",
                // headers:{
                // "Authorization": `Bearer ${localStorage.getItem("token")}`
                //         }}).then(result => result.json()).then(result => {setProduct({productDetail:result,loading:false})
                //     })      
                // console.log(user)
            }   
           }    
            ))
    }

    useEffect(()=>{
        if(user.fullName !== undefined){
           navigate(`/products`)
           }},[user.fullName]) 

    // console.log(user)
  return (
      <Container className="m-5">
        <h2 className ="d-flex justify-content-center">Login</h2>
        <p className="text-center">Not yet registered? <a href="/register">Register</a></p>
          <Row className ="justify-content-center">
          <Col xs={12} md={6}>
            <Form className="m-4" onSubmit={(e)=>registerUser(e)}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value ={email}
                    onChange={(e)=> setemail (e.target.value)} />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} 
                        onChange={(e)=> setpassword (e.target.value)}
                    />
                </Form.Group>

                <Button  className="btn btn-info btn-sm  d-block mx-auto " type="submit" disabled={isDisabled}>
                    Login
                </Button>

            </Form>
          </Col>
      </Row>
      </Container>
  )
}
