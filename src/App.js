import React,{useEffect,useState,useReducer} from 'react'


import HomePage from "./pages/HomePage"
import NotFound from './pages/NotFound'
import Products from "./pages/Products"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Register from "./pages/Register"
import ViewProduct from "./pages/ViewProduct"
import Orders from "./pages/Orders"
import MyOrders from "./pages/MyOrders"
import ProductList from "./pages/ProductList"
import NewProduct from "./pages/NewProduct"
import UpdateProduct from "./pages/UpdateProduct"

//components
import NavBar from './components/NavBar';
import Footer from './components/Footer';
import { initialState, reducer } from './reducer/UserReducer'
//router
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

//context
import{UserProvider} from "./UserContext"
import{ProductProvider} from "./ProductContext"
import{OrderProvider} from "./OrderContext"
import{ReducerProvider} from "./ReducerContext"



function App() {
  const [ state, dispatch ] = useReducer(reducer, initialState)
  const [user,setUser] = useState({
    email: null,
    id:null,
    isAdmin: null
  })
//get user information using bearer token in login
        const [product,setProduct] = useState({
          loading: true,
          productDetail: []
        });
        const strawproduct = product.productDetail
        const [cartItems, setCartItems] = useState([]);
        const onAdd = (strawproduct) => {
          const exist = cartItems.find((x) => x.id === strawproduct.id);
          if (exist) {
            setCartItems(
              cartItems.map((x) =>
                x.id === strawproduct.id ? { ...exist, qty: exist.qty + 1 } : x
              )
            );
          } else {
            setCartItems([...cartItems, { ...strawproduct, qty: 1 }]);
          }
        };
        const onRemove = (strawproduct) => {
          const exist = cartItems.find((x) => x.id === strawproduct.id);
          if (exist.qty === 1) {
            setCartItems(cartItems.filter((x) => x.id !== strawproduct.id));
          } else {
            setCartItems(
              cartItems.map((x) =>
                x.id === strawproduct.id ? { ...exist, qty: exist.qty - 1 } : x
              )
            );
          }
        };

        // console.log(strawproduct)
        const [order,setOrder] = useState([]);
        

          // fetch(`https://zuitt-capstone2-mauro.herokuapp.com/products/active-products`,{
          //   method:"GET",
          //   headers:{
          //   "Authorization": `Bearer ${localStorage.getItem("token")}`
          //           }}).then(result => result.json()).then(result => {setProduct({productDetail:result,loading:false})
          //       })  
          useEffect(() => {
            fetch(`https://zuitt-capstone2-mauro.herokuapp.com/users/profile`,{
              method: 'GET',
              headers:{
                  "Authorization": `Bearer ${localStorage.getItem("token")}`
              }
               }).then(result=>result.json()).then(result=>{
                //  console.log(result)
                const {_id,fullName,isAdmin,email} = result
                //  console.log(_id, fullName, isAdmin)
                if(result.auth == `auth failed`){
                  console.log(`auth failed`)
                }else{
                  setUser({
                    email: email,
                    id: _id,
                    fullName: fullName,
                    isAdmin: isAdmin
                }) 
                console.log(result)
              }})
           },[])

     
           // console.log(cartItems)
         
          
  return (
    <ReducerProvider value={{ state, dispatch }}>
    <UserProvider value={{user, setUser}} >
    <ProductProvider value ={{product, setProduct}}>
    <OrderProvider value ={{order, setOrder}}>
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path='/' element={<HomePage />}/>
        <Route path='/products' element={<Products onAdd={onAdd} />}/>
        <Route path='/login' element={<Login />}/>
        <Route path='/logout' element={<Logout />}/>
        <Route path='/register' element={<Register />}/>
        <Route path='/view/:productId' element={<ViewProduct />}/>
        <Route path='/Orders' element={<Orders />}/>
        <Route path='/MyOrders' element={<MyOrders />}/>
        <Route path='/productList' element={<ProductList />}/>
        <Route path='/newProduct' element={<NewProduct />}/>
        <Route path='/updateProduct/:productId' element={<UpdateProduct />}/>
        <Route path='*' element={<NotFound/>}/>
      </Routes>
      <Footer />
    </BrowserRouter>
    </OrderProvider>
    </ProductProvider>
    </UserProvider>
    </ReducerProvider>
  );
}

export default App;
