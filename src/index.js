import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import {CartProvider} from './components/CartContext.js'



ReactDOM.render(
  <Fragment>
  <CartProvider>
  <App />
  </CartProvider>
  </Fragment>,
  document.getElementById('root')
);



